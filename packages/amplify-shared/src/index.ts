import awsExportsOriginal from './aws-exports';
export const awsExports = awsExportsOriginal;

export * from './API';
export * from './graphql/mutations';
export * from './graphql/queries';
