import ReactDOM from 'react-dom';

import Popup from './Popup';

const Main = () => {
  return (
    <Popup/>
  );
}

ReactDOM.render(<Main/>, document.getElementById('root'));
