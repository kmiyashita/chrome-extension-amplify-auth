import { awsExports } from 'amplify-shared';
import Amplify from 'aws-amplify';
import {useEffect, useState} from 'react';

import SharedAuthStorage from '../common/SharedAuthStorage';
import TodoView from './TodoView';

Amplify.configure({
  ...awsExports,
  Auth: {
    storage: SharedAuthStorage
  }
});

const styles = {
  container: { 
    position: 'fixed' as const,
    bottom: '0px',
    right: '0px',
    width: '400px',
    minHeight: '30px',
    backgroundColor: 'lightgrey',
    zIndex: 1000000000
  }
};

function Overlay() {
  const [signedIn, setSignedIn] = useState(false);

  // Evaluate if you are signed in periodically.
  // You may sign in / out via Chrome extension popup.
  useEffect(() => {
    const intervalId = setInterval(() => {
      const signedIn = SharedAuthStorage.isSignedIn();
      setSignedIn(signedIn);
    }, 3000);
    return () => {
      clearInterval(intervalId);
    }
  }, []);
  
  return (
    <div style={styles.container}>
      {signedIn ? 
        <TodoView/> : 
        <div>Please sign in via Chrome extension's popup.</div>}
    </div>
  );
}

export default Overlay;
